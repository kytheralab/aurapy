# Aurapy

`aurapy` is a python client library for configuration and control of the AURA actuator.

`aurapy` can be used in two ways :
 1. In `console mode` aurapy lets you interact with aura actuators from the command line, without the need to write any python code e.g.:
    - run simple commands (set a speed, position or current target)
    - inspect the aura actuator state (speed, current, position)
    - update the software running on the actuator
 2. from a `python session` aurapy lets you write python code to control aura actuators programmatically. You can access all of the console mode features and more.

**Note:** you can start playing with `aurapy` without physical access to an aura actuator by using a simulated aura instance instead. This is a great way to get the hardware out of your development / test / CI loop. See below for details.

## Table of Contents
- [Aurapy](#aurapy)
  - [Table of Contents](#table-of-contents)
  - [Getting Started](#getting-started)
  - [Console Mode](#console-mode)
  - [Python Scripting](#python-scripting)
  - [Simulated Session](#simulated-session)
  - [Embedded Software Update](#embedded-software-update)
    - [1. From the command-line](#1-from-the-command-line)
    - [2. From the aurapy console](#2-from-the-aurapy-console)
  - [Issues](#issues)


## Getting Started

0. Make sure you have python 3.8+, [pip](https://pip.pypa.io/en/stable/installation/) installed.

1. Install `aurapy`:  
```shell
pip install aurapy --upgrade --extra-index-url https://gitlab.com/api/v4/projects/35011838/packages/pypi/simple
``` 

2. Connect an Aura gateway to your PC using USB, connect one or more Aura actuators to the gateway using CAN.
On linux, if you are not root, you may want to provide read/write rights on the USB port to your user :  `sudo usermod -a -G dialout $USER` and then logout/login.

3. It is good practice to check that the embedded software on the actuator is running the latest version. From the command line:
``` shell
python -m aurapy --update
```

4. To check if your motor is running, you can try the following commands in a python shell.
Make sure there is nothing in the way of the rotor.

```python
from aurapy import demo
demo.start_stop() # spinning the motor - make sure nothing is in the way.
```


## Console Mode
Run the built-in command-line interface:

**Straight from the command line**
```shell
python -m aurapy
```

**From a python session**
```python
import aurapy; aurapy.run()
```

Find available commands with `help` :
```
    st[o]p        -> stops the actuator
    [d]uty 0.2    -> sets pwm to 20%
    [s]peed 100   -> sets speed to 100 rpm
    [p]osition 65 -> sets position to an angle to 65 degrees
    [c]urrent 0.5 -> sets current to 0.5 ampere
    [st]ate       -> prints state
    [v]ersion     -> prints actuator version
    [w]ait 1.5    -> sleeps 1.5 second (useful for chaining commands)
    [u]pdate      -> updates the actuator firmware
    [sa]ve        -> store the temporary configuration in the actuator's flash
    [s]peed_[pid] -> configure the pid parameters when controlling in speed
    [h]elp        -> prints usage
```


## Python Scripting

The following simple python script will connect to an Aura actuator and tell it to run at a specified speed:


Example of actuator control :
```python
import time
from aurapy import AuraClient, rpm
with AuraClient() as aura_client:
    aura_client.set_speed(rpm(100)); # sets a 100 rpm rotational speed target.
    time.sleep(5) # waits 5 seconds

    # automatically disconnects at the end of the with block.
```


Example of state feedback :
```python
import time
from aurapy import AuraClient
with AuraClient() as aura_client:    
    state = aura_client.pull_state()    
    print(f"speed        : {state.speed:5.0f}")
    print(f"board temp   : {state.board_temp:3.2f}")
    print(f"input supply : {state.input_voltage:2.1f}")
    print(f"input current: {state.input_current:2.3f}")
    print(f"timestamp    : {state.timestamp}")
    print(f"dutycycle    : {state.dutycycle:2.2f}")
    print(f"fault        : {state.fault}")
    print(f"position     : {state.position:3.3f}")
    print(f"input power  : {state.input_power:3.3f}")
```


You can explore the AuraClient interface to see what commands are available and
drive the motor with your own python script:


```python
help(aurapy.AuraClient)
```

Take a look at many more scripting examples in `aurapy/demo.py`.
That file should be available in your pip aurapy installation folder. You can find the installation folder by typing : `pip show aurapy` in a terminal.


## Simulated Session
You can specify how many Aura actuators to simulate before starting your aurapy session.
You can then interact with those instances exactly as if your computer was connected to physical actuators.
Here is an example of launching a console session to connect to a simulated aura actuator via an Aura Gateway on the made-up COM_SIM port
```
python -m aurapy --mock-ports COM_SIM=AURA_GW+AURA_M_450:my_aura_actuator
```


You should see the output:
```

Connected to Aura gateway on COM_SIM
Connected to my_aura_actuator on serial port: COM_SIM

Aura command line tool.
    press <CTRL> + C once to stop actuator, twice to exit.
    type help for usage.

```
You can then send a 100 rpm speed target:

```
aura>> speed 100
```
and observe the state:
```
aura>> state
                 temp_board: 24.987976 degC
                temp_mcu: 40 degC
                current: 0.001642 A
                dutycycle: 0.0189247
                speed: 98.573204 rpm
                v_supply: 48.006298 V
                fault: NoFault
                position: 100.118408 deg
```

## Embedded Software Update
Aurapy lets you update the embedded software running on the Aura actuator.
There are two ways to update:

### 1. From the command-line
```shell
python -m aurapy --update
```


### 2. From the aurapy console
Open an aurapy console session with one of the actuators by typing:
```shell
python -m aurapy
```

Then use the `update` command at the prompt:
```
aura>> update
```

## Issues

Please use [gitlab issues](https://gitlab.com/kytheralab/auraclient/-/issues) to report any abnormal behavior or ask questions.



